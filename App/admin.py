from django.contrib import admin
from App.models import UserExtention


class UserExtentionAdmin(admin.ModelAdmin):
    list_display = ('user', 'birthyear', 'profile_picture', 'document_picture')


admin.site.register(UserExtention, UserExtentionAdmin)
