from django.contrib.auth.hashers import make_password
from django.shortcuts import render
from django.contrib.auth.models import User

from App.forms import EditUser, AddUser
from App.models import UserExtention


def index_page(request):
    context = dict()
    context['title'] = 'Index page'
    return render(request, 'index.html', context)


def add_user(request):
    context = dict()
    context['title'] = 'Adding user'

    if request.method == 'POST':
        f = AddUser(request.POST)

        if f.is_valid():
            u_login = f.data['login']
            u_name = f.data['name']
            u_surname = f.data['surname']
            u_pw = f.data['password']
            u_pw_c = f.data['password_confirm']

            if not User.objects.filter(username=u_login).exists():
                if u_pw == u_pw_c:
                    u_pw = make_password(u_pw)
                    new_user = User.objects.create_user(username=u_login, first_name=u_name,
                                                        last_name=u_surname, password=u_pw)
                    new_user.save()
                else:
                    context['errors'] = "Введенные пароли не совпадают"
            else:
                context['errors'] = "Пользователь с таким логином уже существует"

            context['form'] = f
        else:
            context['form'] = f
    else:
        f = AddUser()
        context['form'] = f
    return render(request, 'add_user.html', context)


def user_page(request, uid):
    context = dict()
    context['title'] = 'User page'
    context['id'] = uid
    if User.objects.filter(id=uid).exists():
        user = User.objects.filter(id=uid)[0]
        if not UserExtention.objects.filter(user=user).exists():
            user_extention = UserExtention(user=user)
        else:
            user_extention = UserExtention.objects.filter(user=user)[0]
        context['exist'] = True
        if request.method == 'POST':
            f = EditUser(data=request.POST, files=request.FILES)
            if f.is_valid():
                user_extention.profile_picture = f.files['profile_picture']
                user.first_name = f.data['name']
                user.last_name = f.data['surname']
                user_extention.birthyear = f.data['birthyear']
                user_extention.document_picture = f.files['document_picture']
                context['form'] = f
                context['message'] = 'Данные успешно сохранены!'
            else:
                context['form'] = f
        else:
            context['form'] = EditUser()
        user.save()
        user_extention.save()
    else:
        context['exist'] = False

    if context['exist']:
        user = User.objects.filter(id=uid)[0]
        context['name'] = user.first_name
        context['surname'] = user.last_name
        if UserExtention.objects.filter(user=user).exists():
            user_extention = UserExtention.objects.filter(user=user)[0]
            context['birthyear'] = str(user_extention.birthyear)
            context['photo'] = user_extention.profile_picture
            context['document'] = user_extention.document_picture

    return render(request, 'user_page.html', context)
