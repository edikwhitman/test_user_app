from django.db import models
from django.contrib.auth.models import User


class UserExtention(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    birthyear = models.IntegerField(blank=True, null=True)
    profile_picture = models.ImageField(upload_to='images/', blank=True, null=True)
    document_picture = models.ImageField(upload_to='documents/', blank=True, null=True)
