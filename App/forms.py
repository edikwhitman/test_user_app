from django import forms
from App.models import UserExtention


class EditUser(forms.Form):
    profile_picture = forms.ImageField(label='Фото профиля:')
    name = forms.CharField(max_length=20, label='Имя:', required=False)
    surname = forms.CharField(max_length=20, label='Фамилия', required=False)
    birthyear = forms.IntegerField(label='Год рождения:')
    document_picture = forms.ImageField(label='Фото документа:')


class AddUser(forms.Form):
    login = forms.CharField(max_length=20, label='Логин:')
    name = forms.CharField(max_length=20, label='Имя:')
    surname = forms.CharField(max_length=20, label='Фамилия:')
    password = forms.CharField(widget=forms.PasswordInput(), label='Пароль:')
    password_confirm = forms.CharField(widget=forms.PasswordInput(), label='Подтверждение пароля:')
